@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table border = "1" style="width:100%;">
<tr style="height:30px;background-color:green;color:white;">
<th>Id</th>
<th>Name</th>
<th>Father's Name</th>
<th>Occupation</th>
<th>Address</th>
<th>State</th>
<th>Country</th>
<th>Files</th>
<th>Status</th>
</tr>
@foreach ($members as $student)
<tr>
<td>{{ $student->id }}</td>
<td>{{ $student->name }}</td>
<td>{{ $student->father_name }}</td>
<td>{{ $student->occupation }}</td>
<td>{{ $student->address }}</td>
<td>{{ $student->state }}</td>
<td>{{ $student->country }}</td>
<td>{{ $student->image_path}}</td>
<td><select name="status"><option>Pending</option><option> Approved</option><option> Rejected</option></select></td>
</tr>
@endforeach
</table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
