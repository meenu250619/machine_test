@extends('layouts.app')

@section('content')
<?php
use App\Models\Country;
use App\Models\State;
$countries=Country::all();

$states=State::all();
?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                

                <div class="card-body">
                    <form method="POST" action="student" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" required  autofocus>

                                
                                    
                               
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="father_name" class="col-md-4 col-form-label text-md-right">Father's Name</label>

                            <div class="col-md-6">
                                <input id="father_name" type="text" class="form-control " name="father_name"  required >

                                
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="occupation" class="col-md-4 col-form-label text-md-right">Occupation</label>

                            <div class="col-md-6">
                                <input id="occupation" type="text" class="form-control " name="occupation" required >

                               
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="address" class="col-md-4 col-form-label text-md-right">Address</label>

                            <div class="col-md-6">
                                <input id="address" type="text" class="form-control" name="address" required >
                            </div>
                        </div>
                        
                        <div class="form-group row">
<label for="country" class="col-md-4 col-form-label text-md-right">Country</label>
<div class="col-md-6">
<select name="country"class="form-control" id="country-dropdown">
<option value="">Select Country</option>
@foreach ($countries as $country) 
<option value="{{$country->name}}">
{{$country->name}}
</option>
@endforeach
</select>
</div>
</div>
<div class="form-group row">
<label for="state" class="col-md-4 col-form-label text-md-right">State</label>
<div class="col-md-6">
<select name="state" class="form-control" id="state-dropdown">
<option value="">Select State</option>
@foreach ($states as $state) 
<option value="{{$state->name}}">
{{$state->name}}
</option>
@endforeach
</select>
</div>
</div>                        

                        <div class="form-group row">
                            <label for="image_path" class="col-md-4 col-form-label text-md-right">Upload Files</label>

                            <div class="col-md-6">
                                <input id="image_path" type="file" class="form-control" name="image_path[]" required multiple >
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection