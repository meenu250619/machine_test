<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//student controller
Route::post('/student', 'App\Http\Controllers\StudentController@upload');
Route::get('/uploads', function () {
    return view('/student.index');
});

Route::get('/', function () {
    return view('welcome');
});
Route::get('/user', 'App\Http\Controllers\UserController@index');
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


// user protected routes
Route::group(['middleware' => ['auth', 'user'], 'prefix' => 'user'], function () {
    Route::get('/', function () {
        return view('/student.index');
    });
});

// admin protected routes
Route::group(['middleware' => ['auth', 'admin'], 'prefix' => 'admin'], function () {
    Route::get('/',  function () {
        return view('home');
    });
});

Route::get('home', 'App\Http\Controllers\StudentController@index');

Route::get('/student.index','CountryStateController@index');
Route::post('get-states-by-country','CountryStateController@getState');
