<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;
use DB;
use App\Http\Controllers\Controller;

class StudentController extends Controller
{
    
        public function index(){
            $data=Student::all();
            return view('home',['members'=>$data]);
            
            }
  

    public function upload(Request $request){
        $stud=new Student;
        $stud->name=$request->name;
        $stud->father_name=$request->father_name;
        $stud->occupation=$request->occupation;
        $stud->address=$request->address;
        $stud->state=$request->state;
        $stud->country=$request->country;
       

if($request->hasFile('image_path'))
{
    $img_array=$request->file('image_path');

    $arrlen=count($img_array);
    for($i=0;$i<$arrlen;$i++){
        $image_ext=$img_array[$i]->getClientOriginalExtension();

$image_name=rand(11111,99999).".".$image_ext;
        $destination=public_path('/upload');
        
        $img_array[$i]->move($destination,$image_name);
        $arr[] = $image_name;
   
    
    
    
    }
    

}
    
else
{$picture = '';}
    

$picture = implode(",", $arr); 
    $stud->image_path=$picture;
    $stud->save();


        echo '<script>alert("Inserted Successfully");window.location="http://localhost:8000/index.php";</script>';
          }
}
